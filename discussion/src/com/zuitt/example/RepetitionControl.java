package com.zuitt.example;

public class RepetitionControl {
    public static void main(String[] args) {

        //        [Section] Loops
        //        are control structures that allow code blocks to be executed multiple times

        //        While loop
        //        Allow for repetitive use of code, similar to for-loops, but are usually used for situation where the content to iterate is indefinite.
//        int x = 0;
//
//        while (x < 10) {
//            System.out.println("The current value of x is: " + x);
//            x++;
//        }

//        Do-While Loop
//        Similar to while loop. However, do-while loops always execute atleast once - while loops may not execute/run at all
        int y = 11;
        do {
            System.out.println("The current value of y is: " + y);
            y++;
        } while (y < 10);


//        For-Loop
        for (int i = 0; i < 10; i++) {
            System.out.println("The current value of i is: " + i);
        }

        String[] nameArray = {"John", "Paul", "George", "Ringo"};

        for(String name : nameArray) {
            System.out.println(name);
        }

        String[][] classrooms = new String[3][3];
        //First row
        classrooms[0][0] = "Athos";
        classrooms[0][1] = "Porthos";
        classrooms[0][2] = "Aramis";
        //Second row
        classrooms[1][0] = "Brandon";
        classrooms[1][1] = "JunJun";
        classrooms[1][2] = "Jobert";
        //Third row
        classrooms[2][0] = "Mickey";
        classrooms[2][1] = "Donald";
        classrooms[2][2] = "Goofy";

        System.out.println();

        for (String[] classroom : classrooms) {
            for (String className : classroom) {
                System.out.println(className);
            }
        }
    }

//    For loops for arrays:

}
